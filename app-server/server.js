// Import dependencies
const express = require("express");
const pgp = require("pg-promise")();
var bodyParser = require('body-parser')

const connection = {
    host: "localhost",
    port: 5432,
    database: "postgres",
    user: "postgres",
    password: "postgres",
    max: 30
};

const database = pgp(connection);

// Setup and configure app server
const app = express();
const port = 3000;
app.use(bodyParser.json())


// Sight endpoints (GET, POST, PATCH, DELETE)

/**
 * GET Request
 * Get either all sights (without query)
 * Or get filtered sight (with query: type and or maxPrice)
 */
app.get("/sights", (req, res) => {

    var sqlQuery;

    if (req.query.type && req.query.maxPrice) {
        sqlQuery = "select ST_AsGeoJson(s.*) geojson from sights s where s.type = '" + req.query.type + "' and s.price <= " + req.query.maxPrice;
    } else if (req.query.type) {
        sqlQuery = "select ST_AsGeoJson(s.*) geojson from sights s where s.type = '" + req.query.type + "'";
    } else if (req.query.maxPrice) {
        sqlQuery = "select ST_AsGeoJson(s.*) geojson from sights s where s.price <= " + req.query.maxPrice;
    } else {
        sqlQuery = "select ST_AsGeoJson(s.*) geojson from sights s";
    }

    database.any(sqlQuery)
        .then((data) => {
            let geoJsonResponse = [];
            for (i = 0; i < data.length; i++) {
                geoJsonResponse.push(JSON.parse(data[i].geojson));
            }
            // Response delay of 2 seconds to test the frontend spinner for get all sights
            setTimeout(() => {
                res.status(200).json({
                    type: "FeatureCollection",
                    features: geoJsonResponse
                })
            }, 2000);
        })
        .catch((error) => {
            console.log(error);
            res.status(500).json({ message: "Internal Server Error!", error: error })
        });
})

/**
 * POST Request
 * Creates a new sight
 * Validate if request body includes all required sight properties
 */
app.post("/sights", (req, res) => {

    // Validate request body
    if (!req.body.name || !req.body.lat || !req.body.lon || !req.body.type || !req.body.price) {
        res.status(400).json({ message: "Bad Request!", description: "Please provide all of the required properties: (name, lat, lon, type, price)" });
    } else {
        database.none(`insert into sights (name, lat, lon, type, price, geom) values($1, $2, $3, $4, $5, ST_MakePoint(${req.body.lon}, ${req.body.lat}))`, [req.body.name, req.body.lat, req.body.lon, req.body.type, req.body.price])
            .then(() => {
                res.status(201).json({ message: "Created", data: req.body });
            })
            .catch(error => {
                console.log(error);
                res.status(500).json({ message: "Failed to create sight!", error: error });
            });
    }
})

/**
 * DELETE Request
 * Deletes a sight by its id
 */
app.delete("/sights/:id", (req, res) => {

    database.none("delete from sights where id = $1", [req.params.id])
        .then(() => {
            res.status(204).json();
        })
        .catch(error => {
            res.status(500).json({ message: "Internal Server Error!", error: error });
        })
})

/**
 * PATCH Request
 * Updates a sight by its id
 * Validates if request body includes at least one sight property
 */
app.patch("/sights/:id", (req, res) => {

    var queryStart = "update sights set ";
    var query = queryStart;

    // Validate request body
    if (!req.body.name && !req.body.lat && !req.body.lon && !req.body.type && !req.body.price) {
        res.status(400).json({ message: "Bad Request!" })
    } else {
        if (req.body.name) query += " name = '" + req.body.name + "'";

        if (query !== queryStart && req.body.lat) query += ",";
        if (req.body.lat) query += " lat = " + req.body.lat;

        if (query !== queryStart && req.body.lon) query += ",";
        if (req.body.lon) query += " lon = " + req.body.lon;

        if (query !== queryStart && req.body.type) query += ",";
        if (req.body.type) query += " type = '" + req.body.type + "'";

        if (query !== queryStart && req.body.price) query += ",";
        if (req.body.price) query += " price = " + req.body.price;

        query += " where id = " + req.params.id + ";";

    }

    database.none(query)
        .then(() => {
            res.status(200).json({ message: "Successfully updated user " + req.params.id + "!" });
        })
        .catch(error => {
            res.status(500).json({ message: "Internal Server Error!", error: error });
        })
})


/**
 * Serve static website files
 */
app.use(express.static('../website'));


/**
 * Start http server
 */
app.listen(port, () => {
    console.log("Example app listening at http://localhost:" + port);
})
