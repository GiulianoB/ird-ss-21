let sightListDiv = document.getElementById('sightListDiv');

/**
 * Get All Sights
 * Includes error messages and spinners to symbolize request times
 */
async function getSights() {
  try {
    
    let sightListUl = document.getElementById('sightListsUl');
    if (sightListUl) sightListUl.remove();
    
    disableButton();
    removeErrorMessage();
    appendSpinner();
    let sightsRequest = await fetch('http://localhost:3000/sights');
    let sights = await sightsRequest.json();
    removeSpinner();
    enableButton();
    
    if (sightsRequest.status === 200) {
      let ul = document.createElement('ul');

      ul.setAttribute('id', 'sightListsUl');
      ul.classList.add('list-group');
      sightListDiv.appendChild(ul);

      sights.features.forEach(function (sight) {
        let li = document.createElement('li');
        li.classList.add('list-group-item');
        ul.appendChild(li);
        li.innerHTML += 'Name: ' + sight.properties.name + '<br>' + 'ID: ' + sight.properties.id + '<br>' + 'Type: ' + sight.properties.type;
      });
    
    } else {
      appendErrorMessage();
    }
  } catch (error) {
    appendErrorMessage();
  }
}

function appendErrorMessage() {
  removeErrorMessage();
  let errorMessageElement = document.createElement('p');
  errorMessageElement.setAttribute('id', 'getSightsErrorMessage')
  sightListDiv.appendChild(errorMessageElement);
  errorMessageElement.innerHTML = 'Failed to get sights!';
  errorMessageElement.classList.add('text-danger');
}

function removeErrorMessage() {
  let errorMessageElement = document.getElementById('getSightsErrorMessage');
  if (errorMessageElement) errorMessageElement.remove();
}

function appendSpinner() {
  removeSpinner();
  let spinnerElement = document.createElement('div');
  spinnerElement.setAttribute('id', 'getSightsSpinner');
  //spinnerElement.classList.add('spinner-border')
  spinnerElement.classList = 'spinner-border text-primary';
  let spinnerSpan = document.createElement('span');
  spinnerSpan.classList.add('visually-hidden');
  spinnerElement.appendChild(spinnerSpan);
  sightListDiv.appendChild(spinnerElement);
}

function removeSpinner() {
  let spinnerElement = document.getElementById('getSightsSpinner');
  if (spinnerElement) spinnerElement.remove();
}

function disableButton() {
  let getSightsButton = document.getElementById('getSightsButton');
  getSightsButton.setAttribute('disabled', 'true');
}

function enableButton() {
  let getSightsButton = document.getElementById('getSightsButton');
  getSightsButton.removeAttribute('disabled', 'false');
}

/**
 * Delete Sight
 */
async function deleteSightById() {
  let id = document.getElementById('inputIdToDelete').value;
  await fetch('http://localhost:3000/sights/' + id, { method: 'DELETE' });
  getSights();
}

/**
 * POST Sight
 */
async function postSight() {
  let name = document.getElementById('inputName').value;
  let latitude = document.getElementById('inputLatitude').value;
  let longitude = document.getElementById('inputLongitude').value;
  let type = document.getElementById('inputType').value;
  let price = document.getElementById('inputPrice').value;

  await fetch('http://localhost:3000/sights', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      name: name,
      lat: latitude,
      lon: longitude,
      type: type,
      price: price
    })
  });
  getSights();
}
