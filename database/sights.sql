
-- Drop table if already exists
drop table if exists sights;
-- Create Table
create table sights (
	id serial primary key,
	name text,
	lat numeric,
	lon numeric,
	type text,
	price numeric,
	geom geometry
);

-- Insert test data
insert into sights (name, lat, lon, type, price, geom) 
values('Schlossplatz', 40.902, 9.522, 'culture', 9.99, ST_MakePoint(9.522, 40.902));

insert into sights (name, lat, lon, type, price, geom) 
values('Schöner Berg', 40.902, 9.522, 'culture', 0, ST_MakePoint(9.522, 40.902));

insert into sights (name, lat, lon, type, price, geom) 
values('Mercedes-Benz Museum', 40.902, 9.522, 'fun', 35, ST_MakePoint(9.522, 40.902));

insert into sights (name, lat, lon, type, price, geom) 
values('Coole Sehenswuerdigkeit', 40.902, 14.95, 'fun', 35, ST_MakePoint(9.522, 40.902));

insert into sights (name, lat, lon, type, price, geom) 
values('Fun Sehenswuerdigkeit', 40.902, 9.89, 'fun', 14.99, ST_MakePoint(9.522, 40.902));

insert into sights (name, lat, lon, type, price, geom) 
values('Sehenswürdigkeit via HTTP Request', 40.234, 9.342, 'history', 19.5, ST_MakePoint(9.522, 40.902));

-- Simple select statement
select * from sights;
