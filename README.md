# ird-ss-21

Sights ist eine Anwendung zur Darstellung und Verwaltung von Sehenswürdigkeiten.

Die Anwendung ist als so genannte Micro Service Architecture strukturiert, die aus 4 Komponenten besteht.
- [Datenbankmanagementsystem](database/)
- [Applikations-Server](app-server/)
- [Authentifizierungs-Server](auth-server/)
- [Web-Server](web-server/)

## Voraussetzungen: 

- PostgreSQL >=10
- PostGIS >= 2.6
- NodeJS >= 10

## Datenbankmanagementsystem

### Installation

Die Datenbankinstallationsdateien liegen unter [database/](database/) und lassen sich mit einem SQL Client installieren.

## Applikations-Server

Der Server liegt unter [app-server/](app-server/) und kann wie folgt verwendet werden:

Öffne ein Terminal Programm, navigiere in [app-server/](app-server/) und verwende die folgende Befehle:

### Installation
```
npm install
```

### Starte den Server
```
node server.js
```

### Stoppe den webserver
```
strg + c
```

## Nützliche Entwickler-Tools
- IDE: [Visual Studio Code](https://code.visualstudio.com/)
- SQL Client: [DBeaver](https://dbeaver.io/)
- Http Testing: [Postman](https://www.postman.com/downloads/)


## Quellen zum weiterlernen
- HTTP: [Status Codes](https://developer.mozilla.org/de/docs/Web/HTTP/Status)
- NodeJS: [Express Framework](https://expressjs.com/de/)
- CSS: [Bootstrap Library](https://getbootstrap.com/docs/5.0/components/buttons/)
